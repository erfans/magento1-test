#!/usr/bin/env bash

HOST_NAME=$1
HOST_ALIAS=$2
HOST_IP=$3
MYSQL_VERSION=5.7

echo "Host name: $HOST_NAME"
echo "Host alias: $HOST_ALIAS"
echo "Host ip: $HOST_IP"

echo "=================================================="
echo "Setting up Magento test environment"
echo "- Ubuntu 14.04 LTS"
echo "- Apache 2.4"
echo "- PHP 5.6 7.1 7.2 7.3"
echo "- MySQL $MYSQL_VERSION"
echo "- Composer"
echo "- Magento 1.9.4"
echo "=================================================="


echo "=================================================="
echo "SET LOCALES"
echo "=================================================="

export DEBIAN_FRONTEND=noninteractive

export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_TYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
locale-gen en_US en_US.UTF-8
dpkg-reconfigure locales


echo "=================================================="
echo "RUN UPDATE"
echo "=================================================="

sudo apt-get update
sudo apt-get upgrade


echo "=================================================="
echo "INSTALLING APACHE"
echo "=================================================="

sudo apt-get -y install apache2 libapache2-mod-fcgid

if ! [ -L /var/www ]; then
    rm -rf /var/www
    ln -fs /vagrant /var/www
fi

DEFAULT_VHOST=$(cat <<EOF
    <VirtualHost *:80>
        DocumentRoot "/var/www/src/index/"
        ServerName "$HOST_ALIAS"
        <Directory "/var/www/src/index/">
            AllowOverride All
        </Directory>
        <FilesMatch \.php$>
           SetHandler "proxy:unix:/run/php/php7.3-fpm.sock|fcgi://localhost"
        </FilesMatch>
    </VirtualHost>
EOF
)
sudo echo "$DEFAULT_VHOST" > /etc/apache2/sites-available/000-default.conf

MAGENTO19_VHOST=$(cat <<EOF
    <VirtualHost *:80>
        DocumentRoot "/var/www/src/magento19/"
        ServerName "m19.$HOST_ALIAS"
        <Directory "/var/www/src/magento19/">
            AllowOverride All
        </Directory>
        <FilesMatch \.php$>
           SetHandler "proxy:unix:/run/php/php5.6-fpm.sock|fcgi://localhost"
        </FilesMatch>
        SetEnv MAGE_IS_DEVELOPER_MODE true
    </VirtualHost>
EOF
)
sudo echo "$MAGENTO19_VHOST" > /etc/apache2/sites-available/magento19.conf

echo "ServerName localhost" | sudo tee /etc/apache2/conf-available/localhost.conf

a2enconf localhost
a2enmod actions fcgid alias proxy_fcgi rewrite
a2ensite magento19
service apache2 restart

echo "=================================================="
echo "INSTALLING PHP"
echo "=================================================="

sudo apt-get -y update
sudo add-apt-repository ppa:ondrej/php
sudo apt-get -y update

PHP_VERSION=5.6
sudo apt-get -y install php"$PHP_VERSION" php"$PHP_VERSION"-fpm php"$PHP_VERSION"-curl php"$PHP_VERSION"-cli php"$PHP_VERSION"-mysql php"$PHP_VERSION"-mcrypt php"$PHP_VERSION"-gd php"$PHP_VERSION"-intl php"$PHP_VERSION"-xsl php"$PHP_VERSION"-zip php"$PHP_VERSION"-mbstring php"$PHP_VERSION"-bcmath php"$PHP_VERSION"-soap
PHP_VERSION=7.1
sudo apt-get -y install php"$PHP_VERSION" php"$PHP_VERSION"-fpm php"$PHP_VERSION"-curl php"$PHP_VERSION"-cli php"$PHP_VERSION"-mysql php"$PHP_VERSION"-mcrypt php"$PHP_VERSION"-gd php"$PHP_VERSION"-intl php"$PHP_VERSION"-xsl php"$PHP_VERSION"-zip php"$PHP_VERSION"-mbstring php"$PHP_VERSION"-bcmath php"$PHP_VERSION"-soap
PHP_VERSION=7.2
sudo apt-get -y install php"$PHP_VERSION" php"$PHP_VERSION"-fpm php"$PHP_VERSION"-curl php"$PHP_VERSION"-cli php"$PHP_VERSION"-mysql php"$PHP_VERSION"-gd php"$PHP_VERSION"-intl php"$PHP_VERSION"-xsl php"$PHP_VERSION"-zip php"$PHP_VERSION"-mbstring php"$PHP_VERSION"-bcmath php"$PHP_VERSION"-soap
PHP_VERSION=7.3
sudo apt-get -y install php"$PHP_VERSION" php"$PHP_VERSION"-fpm php"$PHP_VERSION"-curl php"$PHP_VERSION"-cli php"$PHP_VERSION"-mysql php"$PHP_VERSION"-gd php"$PHP_VERSION"-intl php"$PHP_VERSION"-xsl php"$PHP_VERSION"-zip php"$PHP_VERSION"-mbstring php"$PHP_VERSION"-bcmath php"$PHP_VERSION"-soap

phpenmod mbstring
service apache2 restart

chown www-data:www-data /var/lib/php/sessions

echo "=================================================="
echo "INSTALLING COMPOSER"
echo "=================================================="
echo "===== Install composer requirements"
echo "----- Install zip/unzip"
sudo apt-get install zip
sudo apt-get install unzip

echo "----- Install git"
sudo apt-get install git-core

echo "----- Install composer"
curl --silent https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

cd /vagrant/src

echo "=================================================="
echo "INSTALLING MYSQL and CONFIGURE DATABASE"
echo "=================================================="

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt-get -y install mysql-server-"$MYSQL_VERSION"

sudo service mysql restart

sed -i '43 s/^/#/' /etc/mysql/my.cnf
sed -i '47 s/^/#/' /etc/mysql/my.cnf

mysql -u root -proot -Bse "CREATE USER 'magento19'@'%' IDENTIFIED BY 'password';
                           GRANT ALL PRIVILEGES ON *.* TO 'magento19'@'%' WITH GRANT OPTION;
                           FLUSH PRIVILEGES;
                           CREATE DATABASE magento19 character set UTF8 collate utf8_bin;"

mysql -umagento19 -ppassword magento19 < /var/www/vagrant-provisioning/db/magento19.sql

mysql -umagento19 -ppassword magento19 -e "UPDATE core_config_data SET value=\"http://m19.$HOST_ALIAS/\" WHERE path=\"web/unsecure/base_url\"";
mysql -umagento19 -ppassword magento19 -e "UPDATE core_config_data SET value=\"http://m19.$HOST_ALIAS/\" WHERE path=\"web/secure/base_url\"";

sudo service apache2 restart
sudo service mysql restart

echo "=================================================="
echo "CONFIG MAGENTO"
echo "=================================================="

cp /vagrant/vagrant-provisioning/env/m19.xml /vagrant/src/magento19/app/etc/local.xml

echo "=================================================="
echo "CLEANING"
echo "=================================================="
sudo apt-get -y autoremove
sudo apt-get -y autoclean

echo "=================================================="
echo "============= INSTALLATION COMPLETE =============="
echo "=================================================="