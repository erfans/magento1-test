<html>
<head>
    <title>Magento 2 test</title>
</head>
<body>
<h1>Welcome to Magento 2 test environment</h1>

<?php
$versions = ["1.9" => "m19"];
foreach ($versions as $key => $value) : ?>
    <h2> Magento <?php echo $key ?></h2>
    <ul>
        <li><a href="http://<?php echo $value ?>.<?php echo $_SERVER['SERVER_NAME'] ?>"> frontend</a></li>
        <li><a href="http://<?php echo $value ?>.<?php echo $_SERVER['SERVER_NAME'] ?>/admin"> backend</a></li>
    </ul>
<?php endforeach; ?>

</body>
</html>