### Magento 1 test environment 

Including versions 1.9  
The sample data are installed

### How to install
2. run `vagrant up`

### Backend Credentials
username: admin
password: admin123
admin email: admin@domain.com

### PHP Versions
Mangeto 1.9 > PHP5.6  

### Add images
After vagrant up you can download the sample data package and copy
the media folder into the project  
[Sample data](https://magento.com/tech-resources/download#download1933)